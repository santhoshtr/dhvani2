import glob
import logging
import sys
import re
import argparse
import dhvani
from dhvani.voices import DefaultVoice


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="Dhvani TTS"
    )
    parser.add_argument(
        "-i",
        "--infile",
        metavar="INPUT.txt",
        default=sys.stdin,
        help="Input text file",
        nargs="?",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        metavar="OUTPUT.mp3",
        help="Output file for saving the speech",
        default="out.mp3",
        nargs="?",
    )
    parser.add_argument(
        "-d",
        "--database",
        metavar="malayalam.db",
        help="Database containing markov model",
        nargs="?",
    )
    parser.add_argument(
        "-l",
        "--language",
        help="Language"
    )
    return parser.parse_args(args)


def main(args=None):
    options = parse_args(args)
    tts = dhvani.TTS(voice=DefaultVoice())
    with options.infile as fp:
        tts.speak(fp.read(), output_path=options.outfile)


if __name__ == "__main__":
    sys.exit(main())
