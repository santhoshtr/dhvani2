class Syllable(object):
    def __init__(self, text, dpa):
        self.text=text
        self.dpa=dpa

    def __str__(self):
        return "%s %s" % (self.text, self.dpa)