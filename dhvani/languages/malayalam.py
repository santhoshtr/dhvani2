from .language import Language
from ..syllable import Syllable


class Malayalam(Language):
    def __init__(self):
        self.language = "ml"
        self.consonants = 'കഖഗഘങചഛജഝഞടഠഡഢണതഥദധനപഫബഭമയരലവശഷസഹളഴറ'
        self.vowels = 'അആഇഈഉഊഋഎഏഐഒഓഔഔ'
        self.vowel_signs = 'ാിീുൃെേൊോൗൂൈ'
        self.chillus = 'ൻർൽൾൿൺ'
        self.virama = '്'

    def is_chillu(self, letter):
        return letter in self.chillus

    def _syllabify(self, text):
        boundaries = ['ം', 'ഃ', self.virama, 'ൗ'] + [vs for vs in self.vowel_signs]
        limiters = ['.', '\"', '\'', '`', '!', ';', ',', '?']
        chandrakkala = self.virama
        lst_chars = []
        for char in text:
            if char in limiters:
                lst_chars.append(char)
            elif char in boundaries:
                lst_chars[-1] = lst_chars[-1] + char
            else:
                try:
                    if lst_chars[-1][-1] == chandrakkala:
                        lst_chars[-1] = lst_chars[-1] + char
                    else:
                        lst_chars.append(char)
                except IndexError:
                    lst_chars.append(char)

        return lst_chars

    def get_syllables(self, text):
        result = []
        syllables = self._syllabify(text)
        for syl in syllables:
            dpa = self.get_dpa(syl)
            result.append(Syllable(syl, dpa))
        return result

    def get_dpa(self, syllable):
        dpa = []
        syllable_dpa_dict = {
            "അ": "a",
            "ആ": "aa",
            "ഇ": "i",
            "ഈ": "ii",
            "ഉ": "u",
            "ഊ": "uu",
            "ഋ": "rr",
            "എ": "e",
            "ഏ": "ee",
            "ഐ": "ai",
            "ഒ": "o",
            "ഓ": "oo",
            "ഔ": "a",
            "ക": "ka",
            "ങ": "nga",
            "ച": "cha",
            "ഞ": "nja",
            "ട": "ta",
            "ണ": "nha",
            "ത": "tha",
            "ന": "na",
            "പ": "pa",
            "മ": "ma",
            "യ": "ya",
            "ര": "ra",
            "വ": "va",
            "ല": "la",
            "ശ": "ssa",
            "സ": "sa",
            "ഷ": "sha",
            "ഹ": "ha",
            "ള": "lha",
            "ഴ": "zha",
            "റ": "rra",
            "ാ": "aa",
            "ി": "i",
            "ീ": "ii",
            "ു": "u",
            "ൂ": "uu",
            "ൃ": "rr",
            "െ": "e",
            "േ": "ee",
            "ൈ": "ai",
            "ൊ": "o",
            "ോ": "oo",
            "ൗ": "au",
            "ൌ": "au",
            "ൻ": "n~",
            "ർ": "r~",
            "ൽ": "l~",
            "ൾ": "ll~",
            "ൿ": "k~",
            "ൺ": "nh~",
            "്": ""
        }

        for char in syllable:
            if (self.is_vowel_sign(char) or char == self.virama)and dpa[-1] == 'a':
                dpa[-1] = ''
            dpa += syllable_dpa_dict.get(char, char)

        return ''.join(dpa)
