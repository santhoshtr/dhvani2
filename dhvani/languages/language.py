from abc import ABCMeta, abstractmethod
class Language(metaclass=ABCMeta):
    def __init__(self, language=None):
        """Storage base constructor.
        Parameters
        ----------
        path: `str`, optional
        """
        self.language = language

    @abstractmethod
    def get_syllables(self, text):
        pass

    def is_vowel(self, letter):
        return letter in self.vowels

    def is_vowel_sign(self, letter):
        return letter in self.vowel_signs

    def is_consonant(self, letter):
        return letter in self.consonants