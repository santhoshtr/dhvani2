from abc import abstractmethod, ABCMeta
from pydub import AudioSegment
from random import randint
import os


class Voice(metaclass=ABCMeta):
    """Voice base class.
    Attributes
    ----------
    name : `str`
    path : `str`
    """

    def __init__(self, path=None):
        """Storage base constructor.
        Parameters
        ----------
        path: `str`, optional
        """
        self.path = path
        self.name = "Undefined"

    def __eq__(self, voice):
        return self.name == storage.name

    def get_name(self):
        return self.name

    @abstractmethod
    def get_voice_path(self, syllable):
        pass

    @abstractmethod
    def get_file_name(self, syllable):
        pass

    def get_syllable_audio_segments(self, syllables):
        segments = []
        for syllable in syllables:
            fname = self.get_voice_path(syllable)
            if fname:
                segments.append(AudioSegment.from_file(fname))
                print("%s -> %s" % (syllable.text , fname))
            else:
               print("%s -> ?" % syllable.text)
        return segments
