from .voice import Voice
from pkg_resources import resource_filename, resource_exists
import os


class DefaultVoice(Voice):
    def __init__(self):
        self.name = "santhosh"
        self.path = resource_filename(__name__, self.name)

    def get_voice_path(self, syllable):
        path = os.path.join(self.path, self.get_file_name(syllable))
        if os.path.isfile(path):
            return path

    def get_file_name(self, syllable):
        return "%s.mp3" % syllable.dpa
