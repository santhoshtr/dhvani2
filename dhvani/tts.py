import os
import regex
from pydub import AudioSegment
from dhvani.voices import DefaultVoice
from dhvani.languages import Malayalam

class TTS(object):
    def __init__(self, voice=DefaultVoice(), language=Malayalam(), sentence_silence_length_ms=300):
        self.voice = voice
        self.language=language
        self.sentence_silence_length_ms = sentence_silence_length_ms

    def speak(self, text, output_path, output_format="mp3"):
        audio_segments=[]
        for word in regex.split(r'([\.\s]+)', text):
            syllables = self.get_syllables(word.strip())
            audio_segments += self.voice.get_syllable_audio_segments(syllables)
            audio_segments.append( AudioSegment.silent(duration=self.sentence_silence_length_ms))

        combined_sound = sum(audio_segments)
        combined_sound.export(output_path, format=output_format)

    def get_syllables(self, text):
       return self.language.get_syllables(text)